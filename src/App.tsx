import React from 'react';
import {Route, BrowserRouter, Routes} from 'react-router-dom';
import {routesByName} from 'constants/routes';
import HomePage from 'modules/pages/HomePage';
import Page404 from 'modules/pages/Page404';
import UnderConstruction from 'modules/pages/UnderConstruction';
import SignUpContainer from 'modules/auth/signUp/SignUpContainer';

function App() {
  return (
    <BrowserRouter>
      <div className="container">
        <Routes>
          <Route path={routesByName.home} element={<HomePage/>}/>
          <Route path={routesByName.signUp} element={<SignUpContainer/>}/>
          <Route path={routesByName.signIn} element={<UnderConstruction/>}/>
          <Route path="*" element={<Page404/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
