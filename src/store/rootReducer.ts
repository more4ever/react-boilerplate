import authReducer from "../modules/auth/AuthStoreSlice";
import {combineReducers} from '@reduxjs/toolkit'

const rootReducer = combineReducers({
  auth: authReducer,
});

export default rootReducer;
