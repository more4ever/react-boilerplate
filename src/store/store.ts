import appConfig from 'constants/appConfig';
import {configureStore} from "@reduxjs/toolkit";
import rootReducer from "./rootReducer";
import logger from 'redux-logger';


const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => {
    const middlewares = [...getDefaultMiddleware()];
    /**
     * Start logger only on dev environment
     */
    if (appConfig.isDev) {
      middlewares.concat(logger);
    }

    return middlewares;
  },
});

if (appConfig.isDev && module.hot) {
  module.hot.accept('./rootReducer', () => store.replaceReducer(rootReducer))
}


// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export default store;
