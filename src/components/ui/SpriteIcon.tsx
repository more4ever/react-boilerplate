import React, {HTMLProps, SVGProps, useMemo} from 'react';

export enum IconSizesEnum {
  lg = 32,
  md = 24,
  sm = 16,
}

export interface SvgSpriteIconPropTypes extends SVGProps<SVGSVGElement> {
  name: string;
  size?: IconSizesEnum
}

const SpriteIcon = (
  {
    name, size, ...props
  }: SvgSpriteIconPropTypes,
) => {
  const widthHeight = useMemo(
    () => {
      if (!size || !IconSizesEnum[size]) {
        return {}
      }

      const number = IconSizesEnum[size];

      return {
        width: number,
        height: number,
      };
    },
    [size],
  );

  return (
    <svg
      {...widthHeight}
      {...props}
    >
      <use xlinkHref={`#${name}`}/>
    </svg>
  );
};


export default SpriteIcon;
