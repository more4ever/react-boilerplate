import React from 'react';
import ReactDOM from 'react-dom';
import {Provider, ReactReduxContext} from 'react-redux';
import 'styles/index.scss';
import appConfig from 'constants/appConfig';
import {ReactComponent as Sprite} from 'assets/sprite.svg';
import store from 'store/store';
import App from 'App';

const renderApp = () => (
  ReactDOM.render(
    <Provider store={store} context={ReactReduxContext}>
      <Sprite/>
      <App/>
    </Provider>,
    document.getElementById('root'),
  )
);

if (appConfig.isDev && module.hot) {
  module.hot.accept('App', renderApp)
}

renderApp();
