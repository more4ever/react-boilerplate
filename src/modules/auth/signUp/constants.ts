export enum FormFieldsEnum {
  username = 'username',
  gender = 'gender',
  birthDate = 'dob',
  zipCode = 'zip',
}
