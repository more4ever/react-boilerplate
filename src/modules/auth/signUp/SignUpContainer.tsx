import React, {useCallback, useState} from 'react';
import SignUpComponent from 'modules/auth/signUp/SignUpComponent';
import {FormFieldsEnum} from 'modules/auth/signUp/constants';
import AuthService from 'modules/auth/AuthService';
import {routesByName} from 'constants/routes';
import {useNavigate} from "react-router-dom";


const initialState: {
  submitting: boolean,
  error?: string,
  form: Record<FormFieldsEnum, any>,
} = {
  submitting: false,
  error: undefined,
  form: {
    [FormFieldsEnum.username]: '',
    [FormFieldsEnum.gender]: '',
    [FormFieldsEnum.birthDate]: '',
    [FormFieldsEnum.zipCode]: '',
  },
};

const SignUpContainer = () => {
  const navigate = useNavigate();
  const [state, setState] = useState(initialState);

  const onChange = useCallback(({target: {name, value}}) => {
    setState((currentState) => ({...currentState, form: {...currentState.form, [name]: value}}));
  }, [])

  const onSubmit = useCallback(async (event) => {
    event.preventDefault();

    const {
      form: {
        [FormFieldsEnum.gender]: gender,
        ...form
      }, submitting,
    } = state;

    if (submitting) {
      return;
    }

    setState({
      ...state,
      submitting: true,
      error: initialState.error,
    });

    try {
      await AuthService.signUp(
        {
          ...form,
          [FormFieldsEnum.gender]: Number.parseInt(gender, 10),
        },
      );

      navigate(routesByName.home, {replace: true});
    } catch (error) {

      setState({...state, error: (error as Error)?.message, submitting: false});
    }
  }, [state, navigate]);

  const {form, submitting, error} = state;

  return (
    <SignUpComponent
      onChange={onChange}
      onSubmit={onSubmit}
      values={form}
      submitting={submitting}
      error={error}
    />
  );
};

SignUpContainer.propTypes = {};

SignUpContainer.defaultProps = {};

export default SignUpContainer;
