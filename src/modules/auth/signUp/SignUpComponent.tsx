import React, {HTMLProps} from 'react';
import * as PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {routesByName} from 'constants/routes';
import {FormFieldsEnum} from 'modules/auth/signUp/constants';


export type SignUpComponentProps = {
  onChange: HTMLProps<HTMLInputElement>['onChange'];
  onSubmit: HTMLProps<HTMLFormElement>['onSubmit'];
  values: Record<FormFieldsEnum, any>;
  submitting: boolean;
  error?: string;
}

// TODO: final form or formic integration
const SignUpComponent = (
  {
    onChange,
    onSubmit,
    values,
    submitting,
    error,
  }: SignUpComponentProps,
) => (
  <>
    <h1>
      Start your online visit
    </h1>
    <h6>
      This information will help us get you set up with
      {' '}
      a doctor and make sure you are eligible for treatment
    </h6>
    <div className="mb-5">
      Already have a Rize account?
      {' '}
      <Link to={routesByName.signIn}>
        Login
      </Link>
    </div>
    {
      error && (
        <div className="alert alert-danger mb-2" role="alert">
          {error}
        </div>
      )
    }
    <form onSubmit={onSubmit}>
      <div className="form-group row align-items-center">
        <label htmlFor={FormFieldsEnum.username} className="col-auto mb-0">
          Email address
        </label>
        <div className="col">
          <input
            value={values[FormFieldsEnum.username]}
            onChange={onChange}
            type="email"
            className="form-control"
            name={FormFieldsEnum.username}
            id={FormFieldsEnum.username}
          />
        </div>
      </div>
      <div className="form-group row align-items-center">
        <label className="col-auto mb-0">
          Biological Sex
        </label>
        <div className="col">
          <div className="row">
            <div className="col-auto d-flex align-items-center">
              <input
                type="radio"
                name={FormFieldsEnum.gender}
                value="1"
                className="mr-2"
                onChange={onChange}
                checked={values[FormFieldsEnum.gender] === '1'}
                id={`${FormFieldsEnum.gender}-1`}
              />
              <label htmlFor={`${FormFieldsEnum.gender}-1`} className="mb-0">
                Male
              </label>
            </div>
            <div className="col-auto d-flex align-items-center">
              <input
                type="radio"
                name={FormFieldsEnum.gender}
                value="2"
                className="mr-2"
                onChange={onChange}
                id={`${FormFieldsEnum.gender}-2`}
                checked={values[FormFieldsEnum.gender] === '2'}
              />
              <label htmlFor={`${FormFieldsEnum.gender}-2`} className="mb-0">
                Female
              </label>
            </div>
          </div>
        </div>
      </div>
      <div className="form-group row align-items-center">
        <label htmlFor={FormFieldsEnum.birthDate} className="col-auto mb-0">
          Birthdate
        </label>
        <div className="col">
          <input
            type="date"
            className="form-control"
            name={FormFieldsEnum.birthDate}
            id={FormFieldsEnum.birthDate}
            onChange={onChange}
            value={values[FormFieldsEnum.birthDate]}
          />
        </div>
      </div>
      <div className="form-group row align-items-center">
        <label htmlFor={FormFieldsEnum.zipCode} className="col-auto mb-0">
          ZIP Code
        </label>
        <div className="col">
          <input
            name={FormFieldsEnum.zipCode}
            id={FormFieldsEnum.zipCode}
            className="form-control"
            onChange={onChange}
            value={values[FormFieldsEnum.zipCode]}
          />
        </div>
      </div>
      <button type="submit" className="btn btn-primary" disabled={submitting}>
        Submit
      </button>
    </form>
  </>
);

SignUpComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  values: PropTypes.shape({}).isRequired,
  submitting: PropTypes.bool.isRequired,
  error: PropTypes.node,
};

SignUpComponent.defaultProps = {
  error: null,
};

export default SignUpComponent;
