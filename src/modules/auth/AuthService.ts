import BaseAxiosInstance from 'libs/axios/BaseAxiosInstance';

const AuthService = {
  signUp(formValues: Record<any, any>) {
    return BaseAxiosInstance.post('/sign-up', formValues);
  },
};

export default AuthService;
