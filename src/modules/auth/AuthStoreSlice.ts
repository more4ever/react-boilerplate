import {createSlice, PayloadAction} from '@reduxjs/toolkit'

interface AuthSlice {
  isAuth: boolean;
}

const initialState: AuthSlice = {
  isAuth: false
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setIsAuth(state, action: PayloadAction<boolean>) {
      state.isAuth = action.payload;
    }
  }
})


export const {setIsAuth} = authSlice.actions

export default authSlice.reducer;
